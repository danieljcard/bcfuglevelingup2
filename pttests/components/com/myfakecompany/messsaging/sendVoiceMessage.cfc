/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		mockVoice=createMock("components.com.twilio.voice");
		mockVoice.$(method="record",returns=27);
		mockVoice.$(method="wrapitup").$callback(returnStuff);
		mockVoice.$(method="sendit").$callback(checkStub);
		testObj=createObject("components.com.myFakeCompany.Messaging");
		testObj.sendVoiceMessage(person={},message="",tw=mockVoice);

	}

	// executes after all suites+specs in the run() method
	function afterAll(){
	}

/*********************************** BDD SUITES ***********************************/

	function run(){
	
		describe( "My test Suite", function(){
			
			it( "should do something", function(){
                expect( false ).toBeTrue();
			});
			
			it( "should do something else", function(){
                expect( false ).toBeTrue();
			});
			
		});
		
	}

	function returnStuff(args){
		return args;
	}

	function checkStub(person,wrapped){
		//writedump(arguments);
		return (wrapped eq 54);

	}
}
