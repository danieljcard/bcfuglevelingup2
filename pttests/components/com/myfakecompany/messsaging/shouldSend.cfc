/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		testQuery=queryNew("personid","numeric",{personid:1});
	}

	// executes after all suites+specs in the run() method
	function afterAll(){
	}

/*********************************** BDD SUITES ***********************************/

	function run(){
	
		describe( "if messagesLast3Days is 0 and messagesInLastMonth is 0", function(){
			beforeeach(function(){
				testobj=createMock("components.com.myFakeCompany.Messaging");
				testobj.$(method="messagesLast3Days",returns={success:true,data:0});
				testobj.$(method="messagesInLastMonth",returns={success:true,data:0});
				testobj.$(method="notifyTechSupport",return=true);
				testme=testobj.shouldSend(testQuery);
			});
			it( "should return true", function(){
                expect( testme.data ).toBeTrue();
			});
			it("should NOT call notifyTeckSupport",function(){
				expect( testobj.$count("notifyTechSupport")).tobe(0);
			});
		});

		describe("if messagesLast3Days is 1 and messagesInLastMonth is 1",function()
		{
			beforeEach(function(){
				testobj = createMock("components.com.myFakeCompany.Messaging");
				testobj.$(method = "messagesLast3Days", returns = {success:true, data:1});
				testobj.$(method = "messagesInLastMonth", returns = {success:true, data:1});
				testobj.$(method = "notifyTechSupport", return = true);
				testme = testobj.shouldSend(testQuery);
			});
			it("should return false",function()	{
				expect(testme.data).toBeFalse();
			});
		});

		describe("if messagesLast3Days is 0 and messagesInLastMonth is 1",function()
		{
			beforeEach(function(){
				testobj = createMock("components.com.myFakeCompany.Messaging");
				testobj.$(method = "messagesLast3Days", returns = {success:true, data:0});
				testobj.$(method = "messagesInLastMonth", returns = {success:true, data:1});
				testobj.$(method = "notifyTechSupport", returns = true);
				testme = testobj.shouldSend(testQuery);
			});
			it("return .data=false",
				function()
				{
						expect(testme.data).toBeFalse();
				});
			it("shoudl NOT call notifyTechSupport",function(){
				expect(testobj.$count("notifyTechSupport")).tobe(0);
			});
		});

		describe("if messagesLast3Days is 1 and messagesInLastMonth is 0",function()
		{
			beforeEach(function(){
				testobj = createMock("components.com.myFakeCompany.Messaging");
				testobj.$(method = "messagesLast3Days", returns = {success:true, data:1});
				testobj.$(method = "messagesInLastMonth", returns = {success:true, data:0});
				testobj.$(method = "notifyTechSupport", returns = {success:true, data:0});
				testme = testobj.shouldSend(testQuery);
			});
			it("false if messagesLast3Days is 1 and messagesInLastMonth is 0",
				function()
				{
					expect(testme.data).toBeFalse();
				});
			it("should call tech support 1 time",
				function()
				{
						expect(testobj.$count("notifyTechSupport")).toBe(1);
				});
		});
	}
	
}
