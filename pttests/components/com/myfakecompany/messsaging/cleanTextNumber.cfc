/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		testobj=createObject("components.com.myFakeCompany.Messaging");
	}

	// executes after all suites+specs in the run() method
	function afterAll(){
	}

/*********************************** BDD SUITES ***********************************/

	function run(){
	
		describe( "Clean Text Number should...", function(){
			
			it( "clean - from numbers", function(){
				var testme=testobj.cleanTextNumber("123-456-7890");
                expect( testme.data ).toBe("1234567890");
			});
			
			it( "clean ' from numbers", function(){
				var testme=testobj.cleanTextNumber("123'456'7890");
				expect( testme.data ).toBe("1234567890");
			});
			it( "clean * from numbers", function(){
				var testme=testobj.cleanTextNumber("12*345678*90");
				expect( testme.data ).toBe("1234567890");
			});
			it( "clean + from numbers", function(){
				var testme=testobj.cleanTextNumber("+1234+5678+90");
					expect( testme.data ).toBe("1234567890");
			});
		});
		
	}
	
}
