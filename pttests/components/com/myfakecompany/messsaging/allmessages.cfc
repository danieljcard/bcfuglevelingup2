/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		testobj=createObject("components.com.myFakeCompany.Messaging");
		testme=testobj.allMessages();
	}
	// executes after all suites+specs in the run() method
	function afterAll(){
	}
/*********************************** BDD SUITES ***********************************/
	function run(){
		describe( "Query Only", function(){

			it( "return a struct", function(){
					expect( testme ).toBeTypeOf("struct");
			});

			it( "retme.success should be true", function(){
					expect( testme.success ).toBeTrue();
			});

			it("retme.data should be a query",function(){
					expect( testme.data ).tobetypeof("query");
			});

		});
	}
}
