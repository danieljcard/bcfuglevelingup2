/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		testobj=createObject("components.com.myFakeCompany.Messaging");
		testme=testobj.messagesInLastMonth(1);
		writeDump(testme);
	}

	// executes after all suites+specs in the run() method
	function afterAll(){
	}

/*********************************** BDD SUITES ***********************************/

	function run(){

		describe( "messagesInLastMonth...", function(){

			it( "return a struct", function(){
					expect( testme ).toBeTypeOf("struct");
			});

			it( "retme.success should be true", function(){
					expect( testme.success ).toBeTrue();
			});

			it("retme.data should be a number",function(){
					expect( testme.data ).tobetypeof("numeric");
			});

		});
		
	}
	
}
