/**
* My BDD Test
*/
component extends="testbox.system.BaseSpec"{
	
/*********************************** LIFE CYCLE Methods ***********************************/

	// executes before all suites+specs in the run() method
	function beforeAll(){
		testObj=createObject("components.com.myFakeCompany.Messaging");
		testObj.registerSend(1000,1000,0,"My Message");
	}

	// executes after all suites+specs in the run() method
	function afterAll(){
	}

/*********************************** BDD SUITES ***********************************/

	function run(){
	
		describe( "My test Suite", function(){
			
			it( "should do something", function(){
                expect( false ).toBeTrue();
			});
			
			it( "should do something else", function(){
                expect( false ).toBeTrue();
			});
			
		});
		
	}
	
}
