component {
    public struct function allMessages(required messageStatus=0, string datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};

        var qs=new query( datasource=arguments.datasource );
        qs.setSQl( "select * from messages where messagestatus=:status" );
        qs.addParam( name="status",value=arguments.messageStatus );
        retme.data=qs.execute().getResult();

        retme.success=true;
        return retme;
    }

    public struct function allPeople(string datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};
        var qs=new query(datasource=arguments.datasource);
        qs.setSQl("select * from people");
        retme.data=qs.execute().getResult();
        retme.success=true;
        return retme;
    }

    public function sendMessages(message,person){
        var retme={success:false,messages:[],data:{}};

        var shouldSendMessage = shouldSend(person);
        if(!shouldSendMessage){
            registerSend(arguments.person.personid,arguments.message.mesageid,0);
            arrayappend(retme.messages,"Did Not Send, too soon ");
            retme.success=true;
            return retme;
        }

        var personalData=sendDetails(person.personid);
        if(personalData.success){
            var sendTextResult= personalData.data.sendText eq 1 ? sendTextMessage(message,person) : registerSend(arguments.personid,arguments.message.messageid,0,"Too Soon");
            var sendEmailResult= personalData.data.sendEmail eq 1 ? sendEmailMessage(message,person) : registerSend(arguments.personid,arguments.message.messageid,0,"Too Soon");
            var sendVoiceResult= personalData.data.sendVoiceMessage eq 1 ? sendVoiceMessage(message,person) : registerSend(arguments.personid,arguments.message.messageid,0,"Too Soon");
        }


        return retme;
    }

    public function shouldSend(required query person){
        var retme={success:false,messages:[],data:{}};

        var numMessagesLast3Days = messagesLast3Days(arguments.person.personid);
        var numMessagesLastMonth = messagesInLastMonth(Arguments.person.personid);
        if(numMessagesLastMonth.data lt numMessagesLast3Days.data ){
            notifyTechSupport("Check out messages for person #arguments.person.personid#");
        }

        retme.data=false;
        if(!numMessagesLastMonth.data and !numMessagesLast3Days.data){
            retme.data=true;
        }
        retme.success=true;
        return retme;
    }

    public struct function messagesLast3Days(required numeric personid, string datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};
        var qs=new query(datasource=arguments.datasource);
        qs.setSQl("select count(messagetoUserUser) as count from messageToUser where messageToUserUser = :personid and
                    messageToUserDate> :date3DaysAgo");
        qs.addParam(name="personid",value=personid);
        qs.addParam(name="date3DaysAgo",value='#dateformat(dateadd('d',-3,now()),"YYYY-MM-DD")#');
        var allRows=qs.execute().getResult();
        retme.data=allRows.count;
        retme.success=true;
        return retme;
    }

    public struct function messagesInLastMonth(required numeric personid, string datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};
        var qs=new query(datasource=arguments.datasource);
        qs.setSQl("select count(messageToUserUser) as count from messageToUser where messageToUserUser = :personid and
                messageToUserDate>:datelastmonth ");
        qs.addParam(name="personid",value=personid);
        qs.addParam(name="datelastmonth",value='#dateformat(dateadd('m',-1,now()),"YYYY-MM-DD")#');
        var allRows=qs.execute().getResult();
        retme.data=allRows.count;
        retme.success=true;
        return retme;
    }

    public function sendDetails(required numeric personid,string datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};
        var qs=new query(datasource=arguments.datasource);
        qs.setSQl("select * from personalPreferences where personid=:personid");
        qs.addParam(name="personid",value=arguments.personid);
        retme.data=qs.execute().getResult();
        retme.success=true;
        return retme;
    }

    public function sendTextMessage(person,message){
        var retme={success:false,messages:[],data:{}};

        var textNumber=cleanTextNumber(person.phone);
        var sent={success:false};
        if(textNumber.success and len(textNumber.data) eq 10) {
            sent = doTextSend(textNumber, message);
        }
        var registered=registerSend(arguments.personid,arguments.message.messageid,1);

        return retme;
    }

    function cleanTextNumber(required string phoneNumber){
        var retme={success:false,messages:[],data:{}};

        var textNumber=replacenocase(arguments.phoneNumber,"-","","all");
        textNumber=replacenocase(textNumber,"'","","all");
        textNumber=replacenocase(textNumber,"+","","all");
        textNumber=replacenocase(textNumber,"*","","all");
        //var textNumber=rereplace(arguments.phoneNumber,"[^\d]","","all");
        retme.data=textNumber;
        retme.success=true;

        return retme;
    }

    public function doTextSend(required string textNumber,required struct message){
        var retme={success:false,messages:[],data:{}};

        cfhttp(url="sendStuffViaText",result="myRes"){
            cfhttpparam(name="phoneNumber",value=textNumber);
            cfhttpparam(name="messageid",value=message.messageid);
        }
        retme.data=myres;
        return retme;
    }

    public function sendEmailMessage(required struct message,required struct person){
        if(isvalid("email",arguments.person.email)){
            doEmailSend(arguments.person.email,arguments.person.message);
            registerSend(arguments.person.personid,arguments.message.messageid,1);
        }
    }

    public function doEmailSend(emailAddress,message){
        var retme={success:false,messages:[],data:{}};
            cfmail(to=arguments.emailAddress,from="us@thecompany.com",subject=arguments.message.subject,server="MyEmailServer",username="myUsername",password="myPassword"){
                writeoutput(message.message);
            }
        retme.data=true;
        return retme;
    }

    public function registerSend(required numeric personid,required numeric messageid,required numeric sent,string reason='',datasource=application.dsource){
        var retme={success:false,messages:[],data:{}};
        var qs=new query(datasource=arguments.datasource);
        qs.setSQl("insert into messagetouser (messagetoUserUser, messageToUserMessage, messageToUserSent, messageToUserDate, messageToUserSentReason)
            values (:messagetoUserUser, :messageToUserMessage, :messageToUserSent, :messageToUserDate, :messageToUserSentReason)");
        qs.addParam(name="messagetoUserUser",value=arguments.personid);
        qs.addParam(name="messageToUserMessage",value=arguments.messageid);
        qs.addParam(name="messageToUserSent",value=arguments.sent);
        qs.addParam(name="messageToUserDate",value=dateformat(now(),"yyyy-mm-dd"));
        qs.addParam(name="messageToUserSentReason",value=arguments.reason);
        retme.data=qs.execute().getResult();
        retme.success=true;
        return retme;
    }

    public function sendVoiceMessage(person,message,tw){
        var retme={success:false,messages:[],data:{}};

        if(isnull(arguments.tw)){
            arguments.tw=createObject("components.com.twilio.voice");
        }

        var record=tw.record(message);
        var wrapped=tw.wrapitup(record);
        var sent=tw.sendit(arguments.person,wrapped);
        retme.data=sent;
        return retme;
    }
}
