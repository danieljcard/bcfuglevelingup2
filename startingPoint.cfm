<html>
    <head>
        <title>Starting Refactor Point</title>
    </head>
    <body>
        <!--- Get the Message --->
        <cfquery name="theMessage" datasource="#application.dsource#">
            select * from messages where messagestatus=0;
        </cfquery>

        <!--- Get the people who should receive it --->
        <cfquery name="allPeople" datasource="#application.dsource#">
            select * from people
        </cfquery>

        <!--- Loop through the people in the query --->
        <cfoutput query="allPeople">
            <!--- If the person has not received a message in the last three days --->
            <cfquery name="last3" datasource="#application.dsource#">
                select count(messagetoUserUser) as count from messageToUser where messageToUserUser = '#personid#' and
                    messageToUserDate> '#dateformat(dateadd('d',-3,now()),"YYYY-MM-DD")#'
            </cfquery>

            <cfif last3.count eq 0>
                <!--- Get how many messages the person has gotten in the last 4 months --->
                <cfquery name="lastmonth" datasource="#application.dsource#">
                    select count(messageToUserUser) as count from messageToUser where messageToUserUser = '#personid#' and
                messageToUserDate> '#dateformat(dateadd('m',-1,now()),"YYYY-MM-DD")#'
                </cfquery>

                <!--- If they have received less than 4 --->
                <cfif lastmonth.count lt 4>
                    <!--- Get their details --->
                    <cfquery name="details" datasource="#application.dsource#">
                        select * from personalPreferences where personid='#personid#'
                    </cfquery>

                    <!--- If the person has asked for text messages --->
                    <cfif details.sendText[1] eq 1>
                        <!--- Clean the phone number and weed out the bad ones --->
                        <cfset textNumber=replacenocase(details.sendText[1],"-","","all")>
                        <cfset textNumber=replacenocase(details.sendText[1],"'","","all")>
                        <cfset textNumber=replacenocase(details.sendText[1],"+","","all")>
                        <cfset textNumber=replacenocase(details.sendText[1],"*","","all")>
                        <cfif len(textNumber) eq 10>
                            <cfhttp url="sendStuffViaText" result="myRes">
                                <cfhttpparam name="phoneNumber" value="#details.sendText[1]#">
                                <cfhttpparam name="messageid" value="#themessage.messageid#" />
                            </cfhttp>
                        </cfif>
                    </cfif>


                    <!---- If the person has asked for email messsages --->
                    <cfif details.sendEmail[1] eq 1>
                        <cfif isvalid("email",details.sendEmail)>
                            <cfmail to='#email#' from="us@thecompany.com" subject="#themessage.subject#" server="MyEmailServer" username="myUsername" password="myPassword">
                                <cfoutput>#themessage#</cfoutput>
                            </cfmail>
                        </cfif>
                    </cfif>

                    <!--- If the person has requested Voice Messages --->
                    <cfif details.sendVoiceMessage[1] eq 1>
                        <cfset sendTool=createObject("components.com.twilio.voice")>
                        <cfset madewav=sendTool.record(themessage.messageBody)>
                        <cfset fastenWrapper=sendTool.wrapiItUp(madewav.data)>
                        <ftp server="ooga.booga" action="putfile" username="yo" password="oy" />
                    </cfif>

                    <!--- If they've gotten this far, register that the person has received the message --->
                    <cfquery name="itworked" datasource="#application.dsource#">
                            insert into messagetouser (messageToUserUser,messageToUserMessage,messageToUserSent,MessageToUserDate)
                            values ('#personid#','#themessage.messageid#',1,'#dateformat(now(),"yyyy-mm-dd")#');
                    </cfquery>
                <cfelse>
                    <!--- If their were too many messages in the last month --->
                    <cfquery name="TooSoonMonths" datasource="#application.dsource#">
                        insert into messagetouser (messageToUserUser,messageToUserMessage,messageToUserSent,MessageToUserSentReason)
                        values ('#personid#','#themessage.messageid#',0,'Too Soon: Months');
                    </cfquery>
                </cfif>

            <cfelse>
                <!--- If there were too many messages in the last few days --->
                <cfquery name="TooSoonDays" datasource="#application.dsource#">
                    insert into messagetouser (messageToUserUser,messageToUserMessage,messageToUserSent,MessageToUserSentReason)
                    values ('#personid#','#themessage.messageid#',0,'Too Soon: Days');
                </cfquery>

            </cfif>
        </cfoutput>


    </body>
</html>
