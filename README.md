# bcfugLevelingUp1
This is a fictious project contrived to look at various techniques in using testing
tools in various situations including day to day development, specific testing and
then in automated testing.

The scenario is a company who send out periodic communications to its customers via
email, text and voice (Twilio). However, in order to now overwhelm their customers 
with too many intrusions, they space out the messages so no one gets more than one
message every three days and no more than 4 in a month. If it's determined that a 
person is ready for a message, the system them sends out the message via the requested
medium/media (the same message via different methods is allowed). 

The company also gets charged for emails that do not go through because they are malformed
and for texts that fail is they are blank or incomplete. It tries to flag these accounts
so they are not included in future mailings by writing back to the database that the 
account isn't good. 
