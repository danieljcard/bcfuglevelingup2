/****** Object:  Table [dbo].[messages]    Script Date: 11/26/2018 8:50:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[messages](
	[messageid] [int] IDENTITY(1,1) NOT NULL,
	[messagestatus] [int] NULL,
	[messagesendDate] [date] NULL,
	[messageBody] [nvarchar](50) NULL,
	[messageSubject] [nvarchar](100) NULL,
 CONSTRAINT [PK_messages] PRIMARY KEY CLUSTERED
(
	[messageid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[messageToUser]    Script Date: 11/26/2018 8:50:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[messageToUser](
	[messagetoUserUser] [int] NULL,
	[messageToUserMessage] [int] NULL,
	[messageToUserSent] [int] NULL,
	[messageToUserDate] [date] NULL,
	[messageToUserSentReason] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[people]    Script Date: 11/26/2018 8:50:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[people](
	[personid] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nchar](10) NULL,
	[lastname] [nchar](10) NULL,
	[email] [nchar](30) NULL,
	[phone] [nchar](15) NULL,
	[welcomepackageSent] [int] NULL,
	[textbad] [int] NULL,
	[emailbad] [int] NULL,
 CONSTRAINT [PK_people] PRIMARY KEY CLUSTERED
(
	[personid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[personalPreferences]    Script Date: 11/26/2018 8:50:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[personalPreferences](
	[personid] [int] NULL,
	[sendText] [int] NULL,
	[sendEmail] [int] NULL,
	[sendVoiceMessage] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[messages] ON
GO
INSERT [dbo].[messages] ([messageid], [messagestatus], [messagesendDate], [messageBody], [messageSubject]) VALUES (1, 0, CAST(N'2018-11-23' AS Date), N'I need to be sent', N'I am subject to noone!')
GO
SET IDENTITY_INSERT [dbo].[messages] OFF
GO
SET IDENTITY_INSERT [dbo].[people] ON
GO
INSERT [dbo].[people] ([personid], [firstname], [lastname], [email], [phone], [welcomepackageSent], [textbad], [emailbad]) VALUES (1, N'Dan', N'Card', N'myemail@myfakecompany.com', N'123-456-7890 ', NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[people] OFF
GO
INSERT [dbo].[personalPreferences] ([personid], [sendText], [sendEmail], [sendVoiceMessage]) VALUES (1, 1, 1, 1)
GO
